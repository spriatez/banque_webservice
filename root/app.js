const express = require('express');
const bodyParser = require('body-parser');

let jwt = require('jsonwebtoken');
let config = require('./key');
//let accountAction = require('./accountAction');

let AccountModel = require('./Models/AccountModel');
let banque = [];
let account;
let secondAccount = new AccountModel("Mme", "prenom", "nom", "naissance", "adresse", "323232");
let transaction = [];



class HandlerGenerator {
    async login (req, res) {
        try{
            let accountNumber = req.body.accountNumber;
            let password = req.body.password;
            // Recupération des données de account
            let gettedaccountNumber = account.accountNumber;
            let gettedPassword = account.password;

            console.log(accountNumber + " : " + gettedaccountNumber);
            console.log(password + " : " + gettedPassword);

            if (accountNumber && password) {
                if (accountNumber === gettedaccountNumber && password === gettedPassword) {
                    var signOptions = {
                        expiresIn:  "12h",
                        algorithm:  "RS512"
                    };
                    let token = jwt.sign({accountNumber: accountNumber, plafond: account.plafond},
                        config.privateKey,
                        signOptions
                    );
                    account.token = token;
                    // return the JWT token for the future API calls
                    res.json({
                        success: true,
                        message: 'Authentication successful!',
                        token: token
                    });
                } else {
                    res.status(403).json({
                        success: false,
                        message: 'Incorrect username or password'
                    });
                }
            } else {
                res.status(400).json({
                    success: false,
                    message: 'Authentication failed! Please check the request'
                });
            }
        } catch(e) {
            console.error(e);
        }
    }

    getAccountDebug(req, res){
        res.json({"account": account});
    }

    local_money_transaction(req, res){
        let token = req.headers['x-access-token'] || req.headers['authorization'];
        //Récupération dans le body de la date et montant de transaction sur le compte
        let date = (!req.body.date? res.status(400).json({success: false, message: "Date null"}) : req.body.date);
        let montant = (!req.body.montant? res.status(400).json({success: false, message: "Montant null"}) : req.body.montant);

        //Verifiy avec la publicKey
        if (token.startsWith('Bearer ')) {
            // Enlever le Bearer du string reçu
            token = token.slice(7, token.length);
        }
  
        jwt.verify(token, config.publicKey, (err, decoded) => {
            // Désolé pour la cascade de if, le but est d'avoir des messages d'erreurs.
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token is not valid'
                });
            } else {
                // ça fonctionne ça à décodé
                console.log('decoded : ', decoded);
                console.log(banque[0]);
                if(banque[0] == decoded.accountNumber){
                    //Le compte existe dans la banque
                    if(montant <= decoded.plafond){
                        // C'est ok pour le plafond pas dépassé
                        if(montant == 0){
                            return res.status(401).json({
                                success: false,
                                message: 'Unauthorized. Vous n\'avez pas le droit de transferer 0 euros.'
                            });
                        } else {
                            // Problème au niveau du découvert, dans le if suivant il y a un problème d'asynchronité 
                            // Il ne récupère pas le bon solde (il prend le précédent). Si on fait une fois de plus, 
                            // Il n'y a aucun problème et la 401 sort
                            if((account.solde - montant) > 0){
                                montant = parseInt(montant);
                                account.solde += montant;
                            }else {
                                return res.status(401).json({
                                    success: false,
                                    message: 'Unauthorized. Vous n\'avez pas le droit au découvert.'
                                });
                            }
                        }
                        
                    } else {
                        //C'est pas ok, supérieur au plafond
                        return res.status(401).json({
                            success: false,
                            message: 'Unauthorized. Le montant est supérieur au plafond.'
                        });
                    }

                    transaction.push("FROM : " + decoded.accountNumber + " at " + date + " for : " + montant);
                    console.log("FROM : " + decoded.accountNumber + " AT " + date + " FOR : " + montant + " euros");
                    return res.status(200).json({
                        success: true,
                        message: "Transféré avec succes. Solde restant : " + account.solde
                    })
                } else {
                    return res.status(400).json({
                        success: false,
                        message: 'Le compte n\'existe pas dans la banque ..'
                    });
                }
            }
        });
        
    }

    accountInfo(req, res){
        if(account){
            return res.json({
                "accountNumber": account.accountNumber,
                "prenom" : account.prenom,
                "nom": account.nom,
                "plafond" : account.plafond,
                "solde": account.solde
            });
        } else {
            return res.send(404).json({
                success: false,
                message: 'Aucun utilisateur trouvé'
            });
        }
    }

    accountCreation(req, res){
        try {
            
            let civility = (!req.body.civility? res.send(400).json({success: false, message: "Civilite null"}) : req.body.civility);
            let prenom = (!req.body.prenom ? res.send(400).json({success: false, message: "prenom null"}) : req.body.prenom);
            let nom = (!req.body.nom ? res.send(400).json({success: false, message: "nom null"}) : req.body.nom);
            let naissance = (!req.body.naissance ? res.send(400).json({success: false, message: "naissance null"}) : req.body.naissance);
            let adresse = (!req.body.adresse ? res.send(400).json({success: false, message: "adresse null"}) : req.body.adresse);
            let passwordPin = (!req.body.password || isNaN(req.body.password) ? res.send(400).json({success: false, message: "password null/not a number"}) : req.body.password);

            const accountMod = new AccountModel(civility, prenom, nom, naissance, adresse, passwordPin);

            account = accountMod;
            banque.push(account.accountNumber);
            //accountAction.writeInFile(account);

            return res.json({
                success: false,
                message: 'Le compte a été créé avec succès !', 
                account: account
            });
        } catch(e){
            return res.send(400).json({
                success: false,
                message: 'Le compte n\'a pas pu être créé...'
            });
        }
    }

    transaction_local(req, res){
        let transactionToken;

        let accountNumSrc = (!req.body.accountNumSrc ? res.send(400).json({success: false, message: "accountNumSrc null"}) : req.body.accountNumSrc);
        let accountNumDst = (!req.body.accountNumDst ? res.send(400).json({success: false, message: "accountNumDst null"}) : req.body.accountNumDst);
        let montant = (!req.body.montant || isNaN(req.body.montant) ? res.send(400).json({success: false, message: "montant null/not a number"}) : req.body.montant);

        var signOptions = {
            expiresIn:  "12h",
            algorithm:  "RS512"
        };
        let token = jwt.sign({
                accountNumberSrc: accountNumSrc, 
                accountNumberDst: accountNumDst,
                montant: montant,
                plafond: account.plafond},
            config.privateKey,
            signOptions
        );
        transactionToken = token;
        console.log('Token : ', token);
        // return the JWT token for the future API calls
        res.json({
            success: true,
            message: 'Authentication successful!',
            token: token
        });
    }

    transaction_externe(req, res){
        
    }
  
}

// Starting point of the server
function main () {
    let app = express(); // Export app for other routes to use
    let handlers = new HandlerGenerator();
    const port = process.env.PORT || 8000;
    app.use(bodyParser.urlencoded({ // Middleware
        extended: true
    }));
    
    app.use(bodyParser.json());
    // Routes & Handlers
    app.post('/account', handlers.accountCreation);
    app.get('/account', handlers.accountInfo);
    app.post('/login', handlers.login);
    app.post('/local-money-transaction', handlers.local_money_transaction);
    app.post('/transaction-local', handlers.transaction_local);
    app.post('/transaction-externe', handlers.transaction_externe);

    app.get('/getAccountDebug', handlers.getAccountDebug);
    app.listen(port, () => console.log(`Server is listening on port: ${port}`));
}

main();