function accountModel(_civility, _prenom, _nom, _naissance, _adresse, _passwordPin) {
    let accountAction = require('../accountAction');

    let account = {};

    let accountNumber = accountAction.generate();
    let civility = _civility;
    let prenom = _prenom;
    let nom = _nom;
    let naissance = _naissance;
    let adresse = _adresse;
    let passwordPin = _passwordPin;
    let token = null;
    let solde = 250;
    let plafond = 100;

    account = {
        "accountNumber" : accountNumber,
        "civility" : civility,
        "prenom" : prenom,
        "nom" : nom,
        "naissance" : naissance,
        "adresse" : adresse,
        "password" : passwordPin,
        "token" : token,
        "solde" : solde ,
        'plafond' : plafond
    }

    return account;
}

module.exports = accountModel;
