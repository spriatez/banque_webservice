const { generateKeyPair } = require('crypto');

generateKeyPair('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: 'spki',
      format: 'pem'
    },
    privateKeyEncoding: {
      type: 'pkcs8',
      format: 'pem'
    }
  }, (err, publicKey, privateKey) => {
    module.exports.publicKey = publicKey
    module.exports.privateKey = privateKey
  });
